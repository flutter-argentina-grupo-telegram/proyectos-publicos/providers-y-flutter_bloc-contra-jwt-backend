import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/auth/bloc.dart';
import '../../../models/http_exception.dart';
import '../../../utilities/utils.dart';
import '../auth_screen.dart';

class AuthCardBloc extends StatefulWidget {
  const AuthCardBloc({
    Key key,
  }) : super(key: key);

  @override
  _AuthCardBlocState createState() => _AuthCardBlocState();
}

class _AuthCardBlocState extends State<AuthCardBloc>
    with SingleTickerProviderStateMixin {

  final _nombreFocusNode = FocusNode();
  final _apellidoFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();
  final _confirmPasswordFocusNode = FocusNode();

  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.Login;
  Map<String, String> _authData = {
    'nombre': '',
    'apellido': '',
    'email': '',
    'password': '',
  };
  // var _isLoading = false;
  final _passwordController = TextEditingController();
  AnimationController _aniController;
  Animation<Offset> _slideAnimation;
  Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();
    _aniController = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 300,
      ),
    );
    _slideAnimation = Tween<Offset>(
      begin: Offset(0, -1.5),
      end: Offset(0, 0),
    ).animate(CurvedAnimation(
      parent: _aniController,
      curve: Curves.fastOutSlowIn,
    ));
    _opacityAnimation =
        Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _aniController,
      curve: Curves.easeIn,
    ));
  }

  @override
  void dispose() {
    super.dispose();

    _nombreFocusNode.dispose();
    _apellidoFocusNode.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    _confirmPasswordFocusNode.dispose();
    _aniController.dispose();
  }

  Future _submit() async {
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return;
    }
    _formKey.currentState.save();

    try {
      if (_authMode == AuthMode.Login) {
        BlocProvider.of<AuthBloc>(context).add(
          DoSignInEvent(
            _authData['email'], 
            _authData['password'],
          ),
        );
      } else {
        BlocProvider.of<AuthBloc>(context).add(
          DoSignUpEvent(
            _authData['nombre'],
            _authData['apellido'],
            _authData['email'],
            _authData['password'],
          ),
        );
      }
    } on HttpException catch (error) {

      // No debería ejecutarse nunca

      var errorMessage = 'Falló la Autenticación.';

      if (error.toString().contains('EMAIL_EXISTS')) {
        errorMessage = 'Ya existe una cuenta con este Email.';
      } else if (error.toString().contains('INVALID_EMAIL')) {
        errorMessage = 'No es un Email válido.';
      } else if (error.toString().contains('WEAK_PASSWORD')) {
        errorMessage = 'La Contraseña no es muy débil.';
      } else if (error.toString().contains('EMAIL_NOT_FOUND')) {
        errorMessage = 'Credenciales inválidas.';
      } else if (error.toString().contains('INVALID_PASSWORD')) {
        errorMessage = 'Credenciales inválidas.';
      }
      
    } catch (error) {
      const errorMessage =
          'No se pudo realizar la autenticación en este momento. Por favor, intente nuevamente más tarde.';
      mostrarAlerta(context: context, mensaje: errorMessage);
    }

  }

  void _switchAuthMode() async {
    _formKey.currentState.reset();
    if (_authMode == AuthMode.Login) {
      setState(() => _authMode = AuthMode.Signup);
      await _aniController.forward();
      FocusScope.of(context).requestFocus(_nombreFocusNode);
    } else {
      setState(() => _authMode = AuthMode.Login);
      await _aniController.reverse();
      FocusScope.of(context).requestFocus(_emailFocusNode);
    }
  }

  @override
  Widget build(BuildContext context) {
    final isLogin = _authMode == AuthMode.Login;
    final isSignUp = _authMode == AuthMode.Signup;

    final deviceSize = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: 
        AnimatedContainer(
        duration: Duration(milliseconds: 300),
        curve: Curves.easeIn,
        height: isSignUp ? 440 : 260,
        // height: _heightAnimation.value.height,
        constraints: BoxConstraints(minHeight: isSignUp ? 440 : 260),
        width: deviceSize.width * 0.75,
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: BlocBuilder<AuthBloc, AuthState> (
            builder: (context, state) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      constraints: BoxConstraints(
                        minHeight: isSignUp ? 60 : 0,
                        maxHeight: isSignUp ? 120 : 0,
                      ),
                      curve: Curves.easeIn,
                      child: FadeTransition(
                        opacity: _opacityAnimation,
                        child: SlideTransition(
                          position: _slideAnimation,
                          child: TextFormField(
                            enabled: isSignUp,
                            decoration:
                                InputDecoration(labelText: 'Nombre'),
                            obscureText: true,
                            textInputAction: TextInputAction.done,
                            focusNode: _nombreFocusNode,
                            onFieldSubmitted: (value) {
                              FocusScope.of(context).requestFocus(_apellidoFocusNode);
                            },
                            validator: isSignUp
                                ? (value) {
                                    if (value.isEmpty || value.length < 3) {
                                      return 'Debe tener al menos 3 caracteres';
                                    }
                                  }
                                : null,
                            onSaved: (value) {
                              _authData['nombre'] = value;
                            },
                          ),
                        ),
                      ),
                    ),

                    AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      constraints: BoxConstraints(
                        minHeight: isSignUp ? 60 : 0,
                        maxHeight: isSignUp ? 120 : 0,
                      ),
                      curve: Curves.easeIn,
                      child: FadeTransition(
                        opacity: _opacityAnimation,
                        child: SlideTransition(
                          position: _slideAnimation,
                          child: TextFormField(
                            enabled: isSignUp,
                            decoration:
                                InputDecoration(labelText: 'Apellido'),
                            obscureText: true,
                            textInputAction: TextInputAction.done,
                            focusNode: _apellidoFocusNode,
                            onFieldSubmitted: (value) {
                              FocusScope.of(context).requestFocus(_emailFocusNode);
                            },
                            validator: isSignUp
                                ? (value) {
                                    if (value.isEmpty || value.length < 3) {
                                      return 'Debe tener al menos 3 caracteres';
                                    }
                                  }
                                : null,
                            onSaved: (value) {
                              _authData['apellido'] = value;
                            },
                          ),
                        ),
                      ),
                    ),


                    TextFormField(
                      decoration: InputDecoration(labelText: 'E-Mail'),
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                      focusNode: _emailFocusNode,
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(_passwordFocusNode);
                      },
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@')) {
                          return 'Email inválido';
                        }
                      },
                      onSaved: (value) {
                        _authData['email'] = value;
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(labelText: 'Contraseña'),
                      obscureText: true,
                      controller: _passwordController,
                      textInputAction:
                          isLogin ? TextInputAction.done : TextInputAction.next,
                      focusNode: _passwordFocusNode,
                      onFieldSubmitted: (value) {
                        if (isLogin) {
                          _submit();
                        } else {
                          // SignUp
                          FocusScope.of(context)
                              .requestFocus(_confirmPasswordFocusNode);
                        }
                      },
                      validator: (value) {
                        if (value.isEmpty || value.length < 5) {
                          return 'Debe tener al menos 5 caracteres';
                        }
                      },
                      onSaved: (value) {
                        _authData['password'] = value;
                      },
                    ),
                    AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      constraints: BoxConstraints(
                        minHeight: isSignUp ? 60 : 0,
                        maxHeight: isSignUp ? 120 : 0,
                      ),
                      curve: Curves.easeIn,
                      child: FadeTransition(
                        opacity: _opacityAnimation,
                        child: SlideTransition(
                          position: _slideAnimation,
                          child: TextFormField(
                            enabled: isSignUp,
                            decoration:
                                InputDecoration(labelText: 'Confirmar Contraseña'),
                            obscureText: true,
                            textInputAction: TextInputAction.done,
                            focusNode: _confirmPasswordFocusNode,
                            onFieldSubmitted: (value) => _submit(),
                            validator: isSignUp
                                ? (value) {
                                    if (value != _passwordController.text) {
                                      return 'Passwords do not match!';
                                    }
                                  }
                                : null,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    if (state is BusyBlocState)
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: CircularProgressIndicator(),
                      )
                    else
                      Column(
                        children: <Widget>[
                          RaisedButton(
                            child: Text(isLogin ? 'INGRESAR' : 'REGISTRARSE'),
                            onPressed: _submit,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            padding:
                                EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
                            color: Theme.of(context).primaryColor,
                            textColor: Theme.of(context).primaryTextTheme.button.color,
                          ),
                          FlatButton(
                            child: Text('${isLogin ? 'REGISTRARSE' : 'INGRESAR'}'),
                            onPressed: _switchAuthMode,
                            padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            textColor: Theme.of(context).primaryColor,
                          ),
                        ],
                      ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
