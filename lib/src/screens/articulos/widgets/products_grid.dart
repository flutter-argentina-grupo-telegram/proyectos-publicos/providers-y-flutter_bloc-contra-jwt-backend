import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../providers/articulos_provider.dart';
import 'product_item.dart';

class ArticulosGrid extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final articulos = Provider.of<ArticulosProvider>(context).items;

    return GridView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: articulos.length,
      itemBuilder: (context, index) {
        var articulo = articulos[index];
        return Provider.value(
          value: articulo,
          child: ArticuloItem(),
        );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }
}
