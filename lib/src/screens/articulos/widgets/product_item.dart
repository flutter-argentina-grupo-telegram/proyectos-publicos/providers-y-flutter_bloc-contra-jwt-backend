import 'package:bstr_gestion_flutter/src/models/articulo_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../constants.dart';

class ArticuloItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final articulo = Provider.of<ArticuloModel>(context, listen: false);
    // final scaffold = Scaffold.of(context);
    // final theme = Theme.of(context);

    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () => {},
          // onTap: () {
          //   Navigator.of(context).pushNamed(
          //     ArticuloDetailScreen.routeName,
          //     arguments: articulo.id,
          //   );
          // },
          child: Hero(
            tag: articulo.id,
            child: ( articulo.foto == null ) 
              ? Image(image: AssetImage('assets/no-image.png'))
              : FadeInImage(
                image: NetworkImage( '${Constants.baseUrl}/${articulo.foto}' ),
                placeholder: AssetImage('assets/jar-loading.gif'),
                fit: BoxFit.cover,
              ),
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          // leading: IconButton(
          //   icon: Icon(Icons.favorite_border),
          //   onPressed: () => {},
          //   color: Theme.of(context).accentColor,
          // ),
          title: Text(
            articulo.descripcion,
            textAlign: TextAlign.center,
          ),
          // trailing: IconButton(
          //   icon: Icon(Icons.shopping_cart),
          //   onPressed: () => {},
          //   // onPressed: () {
          //   //   carts.addItem(articulo.id, articulo.title, articulo.price);
          //   //   Scaffold.of(context).hideCurrentSnackBar();
          //   //   Scaffold.of(context).showSnackBar(SnackBar(
          //   //     content: Text(
          //   //       'Added item to cart!',
          //   //     ),
          //   //     action: SnackBarAction(
          //   //       label: 'UNDO',
          //   //       onPressed: () {
          //   //         carts.removeSingleItem(articulo.id);
          //   //       },
          //   //     ),
          //   //     duration: Duration(seconds: 2),
          //   //   ));
          //   // },
          //   color: Theme.of(context).accentColor,
          // ),
        ),
      ),
    );
  }
}
