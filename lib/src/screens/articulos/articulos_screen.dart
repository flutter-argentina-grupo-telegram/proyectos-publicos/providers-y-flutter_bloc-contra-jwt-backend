import 'package:bstr_gestion_flutter/src/providers/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/page_model.dart';
import '../../providers/articulos_provider.dart';
import '../../repositories/articulo_repository.dart';
import '../../widgets/app_drawer.dart';
import 'widgets/products_grid.dart';

class ArticulosScreen extends StatefulWidget {
  static const routeName = '/articulos';

  @override
  _ArticulosScreenState createState() => _ArticulosScreenState();
}

class _ArticulosScreenState extends State<ArticulosScreen> {
  var _isInit = false;
  var _isLoading = false;

  PageModel pageModel = PageModel(
    filter: '',
    orderBy: 'descripcion',
    orderDir: 'asc',
    pageNumber: 0,
    pageSize: 10,
  );


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (!_isInit) {
      _isLoading = true;
      Provider.of<ArticulosProvider>(context).fetchArticulos(pageModel: pageModel).then((_) {
        setState(() => _isLoading = false);
      });
      _isInit = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Artículos'),
      ),
      drawer: AppDrawer(),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
            : ArticulosGrid(),
    );
  }
}
