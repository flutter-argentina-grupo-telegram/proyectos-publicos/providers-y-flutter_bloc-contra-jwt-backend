import 'package:bstr_gestion_flutter/src/models/auth_model.dart';
import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class InitialAuthState extends AuthState {
  @override
  List<Object> get props => [];
}

class BusyBlocState extends AuthState {
  @override
  List<Object> get props => [];
}

class SignInOkBlocState extends AuthState {
  final AuthModel authModel;

  SignInOkBlocState(this.authModel);

  @override
  List<Object> get props => [this.authModel];
}


class SignUpOkBlocState extends AuthState {
  final AuthModel authModel;

  SignUpOkBlocState(this.authModel);

  @override
  List<Object> get props => [authModel];
}

class ErrorBlocState extends AuthState {
  final String message;

  ErrorBlocState(this.message);

  @override
  List<Object> get props => [message];
}