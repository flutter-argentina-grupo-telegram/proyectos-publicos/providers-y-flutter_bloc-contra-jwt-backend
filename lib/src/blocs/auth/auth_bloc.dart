import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bstr_gestion_flutter/src/models/auth_model.dart';
import 'package:flutter/foundation.dart';

import './bloc.dart';
import '../../repositories/usuario_repository.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UsuarioRepository usuarioRepository;

  AuthBloc({@required this.usuarioRepository});
  
  @override
  AuthState get initialState => InitialAuthState();

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {

    if (event is DoSignInEvent) {
      print('DoSignInEvent: ${event.email}  -  ${event.password}');
      yield BusyBlocState();

      try {
        AuthModel result = await usuarioRepository.login(
          email: event.email,
          password: event.password,
        );
        print('DoSignInEvent OK: ${result.token}');

        yield SignInOkBlocState(result);
      } catch (e) {
        yield ErrorBlocState('Error en la Autenticación!');
      }
    }

    if (event is DoSignUpEvent) {
      yield BusyBlocState();

      try {
        AuthModel result = await usuarioRepository.register(
          nombre: event.nombre,
          apellido: event.apellido,
          email: event.email,
          password: event.password,
        );
        yield SignInOkBlocState(result);
      } catch (e) {
        yield ErrorBlocState('No se pudo crear la cuenta!');
      }
    }

    if (event is DoSignOutEvent) {
      yield BusyBlocState();

      try {
        await usuarioRepository.logout();
        yield InitialAuthState();
      } catch (e) {
        yield ErrorBlocState('No se pudo crear la sesión!');
      }
    }
    
    if (event is DoTryAutoLoginEvent) {
      yield BusyBlocState();

      try {
        AuthModel result = await usuarioRepository.tryAutoLogin();
        yield SignInOkBlocState(result);
      } catch (e) {
        yield ErrorBlocState('Error en la Autenticación!');
      }
    }
  }
}
