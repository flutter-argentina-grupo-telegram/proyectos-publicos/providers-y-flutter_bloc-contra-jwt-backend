import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class DoSignInEvent extends AuthEvent {
  final String email;
  final String password;

  DoSignInEvent(this.email, this.password);

  @override
  List<Object> get props => [email, password];
}


class DoSignUpEvent extends AuthEvent {
  final String nombre;
  final String apellido;
  final String email;
  final String password;

  DoSignUpEvent(this.nombre, this.apellido, this.email, this.password);

  @override
  List<Object> get props => [nombre, apellido, email, password];
}


class DoSignOutEvent extends AuthEvent {
  @override
  List<Object> get props => null;
}


class DoTryAutoLoginEvent extends AuthEvent {
  @override
  List<Object> get props => null;
}