import 'package:flutter/material.dart';

bool isNumeric( String s ) {

  if ( s.isEmpty ) return false;

  final n = num.tryParse(s);

  return ( n == null ) ? false : true;

}


void mostrarAlerta({BuildContext context, String title = 'Ocurrió un error!', String mensaje, String buttonText = 'Ok'} ) {

  showDialog(
    context: context,
    builder: ( context ) {
      return AlertDialog(
        title: Text(title),
        content: Text(mensaje),
        actions: <Widget>[
          FlatButton(
            child: Text(buttonText),
            onPressed: ()=> Navigator.of(context).pop(),
          )
        ],
      );
    }
  );


}