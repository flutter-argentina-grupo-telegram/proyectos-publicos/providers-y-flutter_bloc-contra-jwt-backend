import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';
import '../screens/articulos/articulos_screen.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          AppBar(
            title: Text('Bstr Gestión'),
            automaticallyImplyLeading: false,
          ),
          ListTile(
            leading: Icon(Icons.shop),
            title: Text('Panel de Control'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.payment),
            title: Text('Ordenes'),
            // onTap: () {
            //   // Navigator.of(context)
            //   //     .pushReplacementNamed(OrdersScreen.routeName);
            //   Navigator.of(context).pushReplacement(CustomRoute(
            //     builder: (context) => OrdersScreen(),
            //   ));
            // },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Admin. Artículos'),
            onTap: () {
              Navigator.of(context)
                  .pushReplacementNamed(ArticulosScreen.routeName);
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Salir'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushReplacementNamed('/');
              Provider.of<Auth>(context, listen: false).logout();
            },
          ),
        ],
      ),
    );
  }
}
