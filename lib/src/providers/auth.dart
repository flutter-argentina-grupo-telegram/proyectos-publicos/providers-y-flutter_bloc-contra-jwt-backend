import 'dart:async';

import 'package:flutter/foundation.dart';

import '../models/auth_model.dart';
import '../repositories/usuario_repository.dart';

class Auth with ChangeNotifier {

  final UsuarioRepository _usuarioRepository;

  String _token;
  DateTime _expiryDate;
  int _userId;
  Timer _authTimer;

  Auth(this._usuarioRepository);

  bool get isAuth {
    return token != null;
  }

  String get token {
    if (_expiryDate != null && _expiryDate.isAfter(DateTime.now())) {
      return _token;
    }
    return null;
  }

  int get userId {
    return _userId;
  }

  Future signUp({String nombre, String apellido, String email, String password}) async {
    try {
      final AuthModel authModel = await _usuarioRepository.register(
        nombre: nombre,
        apellido: apellido,
        email: email,
        password: password,
      );

      _setupAuthModel(authModel);
      _setupAutoLogout();
      notifyListeners();
    } catch (e) {
      print('_authenticate error: $e');
    }
  }

  Future signIn(String email, String password) async {
    try {
      final AuthModel authModel = await _usuarioRepository.login(
        email: email,
        password: password,
      );

      _setupAuthModel(authModel);
      _setupAutoLogout();
      notifyListeners();
    } catch (e) {
      print('_authenticate error: $e');
    }
  }

  Future<bool> tryAutoLogin() async {

    final AuthModel authModel = await _usuarioRepository.tryAutoLogin();

    if (authModel == null) return false;

    _setupAuthModel(authModel);
    _setupAutoLogout();
    notifyListeners();
    
    return true;
  }

  Future logout() async {
    _token = null;
    _expiryDate = null;
    _expiryDate = null;
    _authTimer?.cancel();
    _authTimer = null;

    await _usuarioRepository.logout();

    notifyListeners();
  }

  void _setupAuthModel(AuthModel authModel) {
    _token = authModel.token;
    _userId = authModel.userId;
    _expiryDate = authModel.expiryDate;
  }

  void _setupAutoLogout() {
    _authTimer?.cancel();
    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), () => logout());
  }
}
