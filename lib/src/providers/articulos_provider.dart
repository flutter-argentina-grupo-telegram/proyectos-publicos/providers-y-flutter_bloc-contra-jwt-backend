import 'package:bstr_gestion_flutter/src/models/articulo_model.dart';
import 'package:flutter/cupertino.dart';

import '../models/page_model.dart';
import '../repositories/articulo_repository.dart';
import 'articulo_provider.dart';

class ArticulosProvider extends ChangeNotifier {
  final ArticuloRepository _articuloRepository;

  List<ArticuloModel> _items = [];

  ArticulosProvider(this._articuloRepository, this._items);

  List<ArticuloModel> get items {
    return [..._items];
  }

  Future<void> fetchArticulos({PageModel pageModel}) async {
    var result = await _articuloRepository.articulosGetAll(pageModel: pageModel);

    if (result == null) return;

    _items = result;
    notifyListeners();
  }

  Future<void> addArticulo(ArticuloProvider item) async {
    // final url = '${Constants.DatabaseUrl}/products.json?auth=$authToken';
    // final response = await http.post(url,
    //     body: json.encode({
    //       'title': item.title,
    //       'description': item.description,
    //       'price': item.price,
    //       'imageUrl': item.imageUrl,
    //       'creatorId': userId,
    //     }));

    // if (response.statusCode >= 400) {
    //   throw HttpException(json.decode(response.body)['error']);
    // }

    // final product = Product(
    //   id: json.decode(response.body)['name'],
    //   title: item.title,
    //   description: item.description,
    //   price: item.price,
    //   imageUrl: item.imageUrl,
    // );
    // _items.add(product);
    // notifyListeners();
  }

  Future<void> editArticulo(int id, ArticuloProvider articulo) async {
    // final editedIndex = _items.indexWhere((p) => p.id == id);

    // if (editedIndex >= 0) {
    //   final url = '${Constants.DatabaseUrl}/products/$id.json?auth=$authToken';
    //   final response = await http.patch(
    //     url,
    //     body: json.encode({
    //       'title': product.title,
    //       'description': product.description,
    //       'price': product.price,
    //       'imageUrl': product.imageUrl,
    //     }),
    //   );

    //   if (response.statusCode >= 400) {
    //     throw HttpException('Could not update product.');
    //   }

    //   _items[editedIndex] = product;
    //   notifyListeners();
    // }
  }

  Future<void> deleteArticulo(int id) async {
    // final url = '${Constants.DatabaseUrl}/products/$id.json?auth=$authToken';
    // final existingProductIndex = _items.indexWhere((p) => p.id == id);
    // var existingProduct = _items[existingProductIndex];

    // _items.removeAt(existingProductIndex);
    // notifyListeners();
    // final response = await http.delete(url);

    // // delete does not throw error if we get error status code from the server
    // if (response.statusCode >= 400) {
    //   _items.insert(existingProductIndex, existingProduct);
    //   notifyListeners();
    //   throw HttpException('Could not delete product.');
    // }

    // existingProduct = null;
  }

  ArticuloProvider findById(int id) {
    // return _items.firstWhere((p) => p.id == id);
    return null;
  }
}
