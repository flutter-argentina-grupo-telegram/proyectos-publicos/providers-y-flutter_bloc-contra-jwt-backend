import 'dart:convert';

import 'package:http/http.dart' as http;

import '../constants.dart';
import '../models/articulo_model.dart';
import '../models/http_exception.dart';
import '../models/page_model.dart';

class ArticuloRepository {
  final String _token;

  ArticuloRepository(this._token);


  Future<List<ArticuloModel>> articulosGetAll({PageModel pageModel}) async {
    final url = '${Constants.baseUrlApi}/articulo/getall?filter="${pageModel.filter}"'
                '&orderBy="${pageModel.orderBy}"&orderDir="${pageModel.orderDir}"'
                '&pageNumber=${pageModel.pageNumber.toString()}'
                '&pageSize=${pageModel.pageSize.toString()}';
    
    var response = await http.get(url,
          headers: {"Content-Type": "application/json",
                    "Authorization": "Bearer $_token"}
    );

    // print('response.statusCode: ${response.statusCode}');
    // print('response.body: ${response.body}');

    if (response.statusCode == 401) 
      throw HttpException('Fallo de Autorización - Sesión expirada');

    Map<String, dynamic> responseData = json.decode( response.body );

    // print('responseData: $responseData');

    if (responseData['errors'] != null || response.statusCode != 200) {
      throw HttpException(responseData['errors']['message']);
    }

    final List<ArticuloModel> articulos = new List();

    final data = responseData['data'];

    if (data == null) return [];

    data.forEach((art) {
      ArticuloModel articulo = ArticuloModel.fromJson(art);

      articulos.add(articulo);
    });

    return articulos;
  }

}