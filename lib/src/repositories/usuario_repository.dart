import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';
import '../models/auth_model.dart';
import '../models/http_exception.dart';

class UsuarioRepository {

  Future<AuthModel> _authenticate({String url, Map<String, dynamic> data}) async {

    var response = await http.post(
          url,
          headers: {"Content-Type": "application/json"},
          body: json.encode( data )
        );

    Map<String, dynamic> responseData = json.decode( response.body );

    print('Response status: ${response.statusCode}');
    print(responseData);

    if (responseData['errors'] != null || (response.statusCode != 200 && response.statusCode != 201)) {
      throw HttpException(responseData['errors']['message']);
    }

    final result = AuthModel(
      token: responseData['token'],
      userId: responseData['id'],
      expiryDate: DateTime.now().add(
        Duration(
          seconds: responseData['expiresIn'],
        )
      ),
    );


    final prefs = await SharedPreferences.getInstance();
    final userData = authModelToJson(result);
    prefs.setString(Constants.userDataKey, userData);

    return result;
  }


  Future<AuthModel> login({String email, String password}) async {
    print('UsuarioRepository - Login: $email  -  $password');

    try {
      final authData = {
        'email'     : email,
        'password'  : password,
      };

      final url = '${Constants.baseUrlApi}/auth/login';

      return _authenticate(url: url, data: authData);
    } catch (e) {
      rethrow;
    }
  }


  Future<AuthModel> register({String nombre, String apellido, String email, String password}) async {
    try {
      final authData = {
        'nombre'    : nombre,
        'apellido'  : apellido,
        'email'     : email,
        'password'  : password,
      };

      final url = '${Constants.baseUrlApi}/auth/register';

      return _authenticate(url: url, data: authData);
    } catch (e) {
      rethrow;
    }
  }

  Future<AuthModel> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    // prefs.setString(Constants.userDataKey, userData);

    if (!prefs.containsKey(Constants.userDataKey)) return null;

    final extractedUserData = json.decode(prefs.getString(Constants.userDataKey));
    // final expiryDate = DateTime.parse(extractedUserData['expiryDate']);
    final AuthModel result = AuthModel.fromJson(extractedUserData);

    if (result.expiryDate.isBefore(DateTime.now())) {
      return null;
    }

    return result;
  }

  Future<void> logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(Constants.userDataKey);
  }
}