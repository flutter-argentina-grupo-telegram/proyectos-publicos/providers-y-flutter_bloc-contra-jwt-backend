import 'dart:convert';

import 'grupo_model.dart';


LineaModel lineaModelFromJson(String str) => LineaModel.fromJson(json.decode(str));

String lineaModelToJson(LineaModel data) => json.encode(data.toJson());

class LineaModel {
    int id;
    String descripcion;
    String codigo;
    int grupoId;
    GrupoModel grupo;

    LineaModel({
        this.id,
        this.descripcion,
        this.codigo,
        this.grupoId,
        this.grupo,
    });

    factory LineaModel.fromJson(Map<String, dynamic> json) => LineaModel(
        id: json["id"],
        descripcion: json["descripcion"],
        codigo: json["codigo"],
        grupoId: json["grupoId"],
        grupo: GrupoModel.fromJson(json["grupo"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "codigo": codigo,
        "grupoId": grupoId,
        "grupo": grupo.toJson(),
    };
}
