import 'dart:convert';

import 'articulo_componente_model.dart';
import 'articulo_deposito_model.dart';
import 'articulo_foto_model.dart';
import 'linea_model.dart';
import 'marca_model.dart';
import 'porcentaje_model.dart';


ArticuloModel articuloModelFromJson(String str) => ArticuloModel.fromJson(json.decode(str));

String articuloModelToJson(ArticuloModel data) => json.encode(data.toJson());

class ArticuloModel {
    int     id;
    String  descripcion;
    String  codigo;
    String  codigoBarra;
    double  costo;
    double  precioVenta;
    bool    forzarPrecioVenta;
    bool    aplicarDescuento;
    bool    activo;
    bool    controlaStock;
    bool    esCompuesto;
    bool    esProduccionPropia;
    double  puntoDeReposicion;
    double  stockMinimo;
    double  stockMaximo;
    dynamic foto;

    double  costoFinal;
    double  netoGrabado;
    double  precioFinal;

    int             lineaId;
    LineaModel      linea;
    int             marcaId;
    MarcaModel      marca;
    int             porcentajeIvaId;
    PorcentajeModel porcentajeIva;
    int             porcentajeUtilidadId;
    PorcentajeModel porcentajeUtilidad;
    
    List<ArticuloFotoModel>       articuloFotos;
    List<ArticuloDepositoModel>   articuloDepositos;
    List<ArticuloComponenteModel> articulosComponentes;

    ArticuloModel({
        this.id,
        this.descripcion,
        this.codigo,
        this.codigoBarra,
        this.costo,
        this.precioVenta,
        this.forzarPrecioVenta,
        this.aplicarDescuento,
        this.activo,
        this.controlaStock,
        this.esCompuesto,
        this.esProduccionPropia,
        this.puntoDeReposicion,
        this.stockMinimo,
        this.stockMaximo,
        this.foto,

        this.costoFinal,
        this.netoGrabado,
        this.precioFinal,

        this.lineaId,
        this.linea,
        this.marcaId,
        this.marca,
        this.porcentajeIvaId,
        this.porcentajeIva,
        this.porcentajeUtilidadId,
        this.porcentajeUtilidad,
        this.articuloFotos,
        this.articuloDepositos,
        this.articulosComponentes,
    });

    factory ArticuloModel.fromJson(Map<String, dynamic> json) => ArticuloModel(
        id: json["id"],
        descripcion: json["descripcion"],
        codigo: json["codigo"],
        codigoBarra: json["codigoBarra"],
        costo: json["costo"],
        precioVenta: json["precioVenta"],
        forzarPrecioVenta: json["forzarPrecioVenta"],
        aplicarDescuento: json["aplicarDescuento"],
        activo: json["activo"],
        controlaStock: json["controlaStock"],
        esCompuesto: json["esCompuesto"],
        esProduccionPropia: json["esProduccionPropia"],
        puntoDeReposicion: json["puntoDeReposicion"],
        stockMinimo: json["stockMinimo"],
        stockMaximo: json["stockMaximo"],
        foto: json["foto"],
        costoFinal: json["costoFinal"],
        netoGrabado: json["netoGrabado"],
        precioFinal: json["precioFinal"],
        lineaId: json["lineaId"],
        linea: LineaModel.fromJson(json["linea"]),
        marcaId: json["marcaId"],
        marca: MarcaModel.fromJson(json["marca"]),
        porcentajeIvaId: json["porcentajeIvaId"],
        porcentajeIva: PorcentajeModel.fromJson(json["porcentajeIva"]),
        porcentajeUtilidadId: json["porcentajeUtilidadId"],
        porcentajeUtilidad: PorcentajeModel.fromJson(json["porcentajeUtilidad"]),
        articuloFotos: List<ArticuloFotoModel>.from(json["articuloFotos"].map((x) => ArticuloFotoModel.fromJson(x))),
        articuloDepositos: List<ArticuloDepositoModel>.from(json["articuloDepositos"].map((x) => ArticuloDepositoModel.fromJson(x))),
        articulosComponentes: List<ArticuloComponenteModel>.from(json["articulosComponentes"].map((x) => ArticuloComponenteModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "codigo": codigo,
        "codigoBarra": codigoBarra,
        "costo": costo,
        "precioVenta": precioVenta,
        "forzarPrecioVenta": forzarPrecioVenta,
        "aplicarDescuento": aplicarDescuento,
        "activo": activo,
        "controlaStock": controlaStock,
        "esCompuesto": esCompuesto,
        "esProduccionPropia": esProduccionPropia,
        "puntoDeReposicion": puntoDeReposicion,
        "stockMinimo": stockMinimo,
        "stockMaximo": stockMaximo,
        "foto": foto,
        "costoFinal": costoFinal,
        "netoGrabado": netoGrabado,
        "precioFinal": precioFinal,
        "lineaId": lineaId,
        "linea": linea.toJson(),
        "marcaId": marcaId,
        "marca": marca.toJson(),
        "porcentajeIvaId": porcentajeIvaId,
        "porcentajeIva": porcentajeIva.toJson(),
        "porcentajeUtilidadId": porcentajeUtilidadId,
        "porcentajeUtilidad": porcentajeUtilidad.toJson(),
        "articuloFotos": List<ArticuloFotoModel>.from(articuloFotos.map((x) => x)),
        "articuloDepositos": List<ArticuloDepositoModel>.from(articuloDepositos.map((x) => x)),
        "articulosComponentes": List<ArticuloComponenteModel>.from(articulosComponentes.map((x) => x)),
    };
}



