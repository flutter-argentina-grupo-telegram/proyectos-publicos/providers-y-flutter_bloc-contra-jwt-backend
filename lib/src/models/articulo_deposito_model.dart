import 'dart:convert';

import 'deposito_model.dart';


ArticuloDepositoModel articuloDepositoModelFromJson(String str) => ArticuloDepositoModel.fromJson(json.decode(str));

String articuloDepositoModelToJson(ArticuloDepositoModel data) => json.encode(data.toJson());


class ArticuloDepositoModel {
    int articuloId;
    int depositoId;
    DepositoModel deposito;
    int stock;
    int comprometido;
    int pedidoAProveedor;
    int enProduccion;

    ArticuloDepositoModel({
        this.articuloId,
        this.depositoId,
        this.deposito,
        this.stock,
        this.comprometido,
        this.pedidoAProveedor,
        this.enProduccion,
    });

    factory ArticuloDepositoModel.fromJson(Map<String, dynamic> json) => ArticuloDepositoModel(
        articuloId: json["articuloId"],
        depositoId: json["depositoId"],
        deposito: DepositoModel.fromJson(json["deposito"]),
        stock: json["stock"],
        comprometido: json["comprometido"],
        pedidoAProveedor: json["pedidoAProveedor"],
        enProduccion: json["enProduccion"],
    );

    Map<String, dynamic> toJson() => {
        "articuloId": articuloId,
        "depositoId": depositoId,
        "deposito": deposito.toJson(),
        "stock": stock,
        "comprometido": comprometido,
        "pedidoAProveedor": pedidoAProveedor,
        "enProduccion": enProduccion,
    };
}
