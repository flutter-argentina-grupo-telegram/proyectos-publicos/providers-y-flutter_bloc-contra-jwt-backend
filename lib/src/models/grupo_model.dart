import 'dart:convert';

GrupoModel grupoModelFromJson(String str) => GrupoModel.fromJson(json.decode(str));

String grupoModelToJson(GrupoModel data) => json.encode(data.toJson());

class GrupoModel {
    int id;
    String descripcion;
    String codigo;

    GrupoModel({
        this.id,
        this.descripcion,
        this.codigo,
    });

    factory GrupoModel.fromJson(Map<String, dynamic> json) => GrupoModel(
        id: json["id"],
        descripcion: json["descripcion"],
        codigo: json["codigo"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "codigo": codigo,
    };
}
