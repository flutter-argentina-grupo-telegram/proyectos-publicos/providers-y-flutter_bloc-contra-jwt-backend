// To parse this JSON data, do
//
//     final pageModel = pageModelFromJson(jsonString);

import 'dart:convert';

PageModel pageModelFromJson(String str) => PageModel.fromJson(json.decode(str));

String pageModelToJson(PageModel data) => json.encode(data.toJson());

class PageModel {

    String filter;
    String orderBy;
    String orderDir;
    int pageNumber;
    int pageSize;

    PageModel({
        this.filter     = '',
        this.orderBy    = 'descripcion',
        this.orderDir   = 'asc',
        this.pageNumber = 0,
        this.pageSize   = 10,
    });

    factory PageModel.fromJson(Map<String, dynamic> json) => new PageModel(
        filter      : json["filter"],
        orderBy     : json["orderBy"],
        orderDir    : json["orderDir"],
        pageNumber  : json["pageNumber"],
        pageSize    : json["pageSize"],
    );

    Map<String, dynamic> toJson() => {
        "filter"      : filter,
        "orderBy"     : orderBy,
        "orderDir"    : orderDir,
        "pageNumber"  : pageNumber,
        "pageSize"    : pageSize,
    };
}
