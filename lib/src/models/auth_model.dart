import 'dart:convert';

import 'package:equatable/equatable.dart';

AuthModel authModelFromJson(String str) => AuthModel.fromJson(json.decode(str));

String authModelToJson(AuthModel data) => json.encode(data.toJson());

class AuthModel extends Equatable {
    String token;
    int userId;
    DateTime expiryDate;

    AuthModel({
        this.token = '',
        this.userId,
        this.expiryDate,
    });

    factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        token: json["token"],
        userId: int.parse(json["userId"]),
        expiryDate: DateTime.parse(json['expiryDate']),
    );

    Map<String, dynamic> toJson() => {
        "token": token,
        "userId": userId.toString(),
        "expiryDate": expiryDate.toIso8601String(),
    };

  @override
  List<Object> get props => [this.token, this.userId, this.expiryDate];
}
