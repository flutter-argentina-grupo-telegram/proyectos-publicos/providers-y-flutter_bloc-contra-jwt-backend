import 'dart:convert';

DepositoModel depositoModelFromJson(String str) => DepositoModel.fromJson(json.decode(str));

String depositoModelToJson(DepositoModel data) => json.encode(data.toJson());

class DepositoModel {
    int id;
    String descripcion;
    String domicilio;
    String codigoPostal;
    String telefono1;
    String telefono2;
    String telefonoFax;
    String email;

    DepositoModel({
        this.id,
        this.descripcion,
        this.domicilio,
        this.codigoPostal,
        this.telefono1,
        this.telefono2,
        this.telefonoFax,
        this.email,
    });

    factory DepositoModel.fromJson(Map<String, dynamic> json) => DepositoModel(
        id: json["id"],
        descripcion: json["descripcion"],
        domicilio: json["domicilio"],
        codigoPostal: json["codigoPostal"],
        telefono1: json["telefono1"],
        telefono2: json["telefono2"],
        telefonoFax: json["telefonoFax"],
        email: json["email"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "domicilio": domicilio,
        "codigoPostal": codigoPostal,
        "telefono1": telefono1,
        "telefono2": telefono2,
        "telefonoFax": telefonoFax,
        "email": email,
    };
}
