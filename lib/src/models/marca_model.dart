import 'dart:convert';

MarcaModel marcaModelFromJson(String str) => MarcaModel.fromJson(json.decode(str));

String marcaModelToJson(MarcaModel data) => json.encode(data.toJson());

class MarcaModel {
    int id;
    String descripcion;
    String codigo;

    MarcaModel({
        this.id,
        this.descripcion,
        this.codigo,
    });

    factory MarcaModel.fromJson(Map<String, dynamic> json) => MarcaModel(
        id: json["id"],
        descripcion: json["descripcion"],
        codigo: json["codigo"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "codigo": codigo,
    };
}
