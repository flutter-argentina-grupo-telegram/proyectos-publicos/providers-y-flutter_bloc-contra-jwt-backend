import 'dart:convert';

import 'articulo_model.dart';


ArticuloComponenteModel articuloComponenteModelFromJson(String str) => ArticuloComponenteModel.fromJson(json.decode(str));

String articuloComponenteModelToJson(ArticuloComponenteModel data) => json.encode(data.toJson());

class ArticuloComponenteModel {
    int articuloCompuestoId;
    int articuloComponenteId;
    double cantidad;
    bool intervienePrecio;
    ArticuloModel articuloComponente;

    ArticuloComponenteModel({
        this.articuloCompuestoId,
        this.articuloComponenteId,
        this.cantidad,
        this.intervienePrecio,
        this.articuloComponente,
    });

    factory ArticuloComponenteModel.fromJson(Map<String, dynamic> json) => ArticuloComponenteModel(
        articuloCompuestoId: json["articuloCompuestoId"],
        articuloComponenteId: json["articuloComponenteId"],
        cantidad: json["cantidad"],
        intervienePrecio: json["intervienePrecio"],
        articuloComponente: ArticuloModel.fromJson(json["articuloComponente"]),
    );

    Map<String, dynamic> toJson() => {
        "articuloCompuestoId": articuloCompuestoId,
        "articuloComponenteId": articuloComponenteId,
        "cantidad": cantidad,
        "intervienePrecio": intervienePrecio,
        "articuloComponente": articuloComponente.toJson(),
    };
}
