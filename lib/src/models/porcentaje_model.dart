import 'dart:convert';

PorcentajeModel porcentajeModelFromJson(String str) => PorcentajeModel.fromJson(json.decode(str));

String porcentajeModelToJson(PorcentajeModel data) => json.encode(data.toJson());

class PorcentajeModel {
    int id;
    String descripcion;
    String codigo;
    String porcentajeTexto;
    double coeficiente;
    bool esIva;
    bool esIvaAdicional;
    bool esUtilidad;
    bool esRecargo;
    bool esDescuentoCliente;
    bool esComisionTarjeta;
    int columnaIva;

    PorcentajeModel({
        this.id,
        this.descripcion,
        this.codigo,
        this.porcentajeTexto,
        this.coeficiente,
        this.esIva,
        this.esIvaAdicional,
        this.esUtilidad,
        this.esRecargo,
        this.esDescuentoCliente,
        this.esComisionTarjeta,
        this.columnaIva,
    });

    factory PorcentajeModel.fromJson(Map<String, dynamic> json) => PorcentajeModel(
        id: json["id"],
        descripcion: json["descripcion"],
        codigo: json["codigo"],
        porcentajeTexto: json["porcentajeTexto"],
        coeficiente: json["coeficiente"].toDouble(),
        esIva: json["esIva"],
        esIvaAdicional: json["esIvaAdicional"],
        esUtilidad: json["esUtilidad"],
        esRecargo: json["esRecargo"],
        esDescuentoCliente: json["esDescuentoCliente"],
        esComisionTarjeta: json["esComisionTarjeta"],
        columnaIva: json["columnaIva"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "codigo": codigo,
        "porcentajeTexto": porcentajeTexto,
        "coeficiente": coeficiente,
        "esIva": esIva,
        "esIvaAdicional": esIvaAdicional,
        "esUtilidad": esUtilidad,
        "esRecargo": esRecargo,
        "esDescuentoCliente": esDescuentoCliente,
        "esComisionTarjeta": esComisionTarjeta,
        "columnaIva": columnaIva,
    };
}
