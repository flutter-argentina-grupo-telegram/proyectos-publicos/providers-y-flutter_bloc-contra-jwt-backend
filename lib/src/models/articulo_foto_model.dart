class ArticuloFotoModel {
    int id;
    String foto;
    bool esPrincipal;
    int articuloId;

    ArticuloFotoModel({
        this.id,
        this.foto,
        this.esPrincipal,
        this.articuloId,
    });

    factory ArticuloFotoModel.fromJson(Map<String, dynamic> json) => ArticuloFotoModel(
        id: json["id"],
        foto: json["foto"],
        esPrincipal: json["esPrincipal"],
        articuloId: json["articuloId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "foto": foto,
        "esPrincipal": esPrincipal,
        "articuloId": articuloId,
    };
}
