import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/articulos_provider.dart';
import 'providers/auth.dart';
import 'repositories/articulo_repository.dart';
import 'repositories/usuario_repository.dart';
import 'screens/articulos/articulos_screen.dart';
import 'screens/auth/auth_screen.dart';
import 'screens/home_screen.dart';
import 'screens/splash_screen.dart';
import 'utilities/custom_route.dart';

class MyAppProvider extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider.value(value: UsuarioRepository()),
        ChangeNotifierProxyProvider<UsuarioRepository, Auth>(
          create: (_) => null,
          update: (context, usuarioRepository, prevAuth) => Auth(
            usuarioRepository
          ),
        ),
        ProxyProvider<Auth, ArticuloRepository>(
          create: (_) => null,
          update: (context, auth, artRepository) => ArticuloRepository(
            auth.token
          ),
        ),
        ChangeNotifierProxyProvider<ArticuloRepository, ArticulosProvider>(
          create: (_) => null,
          update: (context, artRepository, prevArtProvider) => ArticulosProvider(
            artRepository,
            prevArtProvider == null ? [] : prevArtProvider.items,
          ),
        ),
      ],
      child: Consumer<Auth>(
        builder: (context, auth, child) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Bstr Gestión',
            theme: ThemeData(
              primarySwatch: Colors.blue,
              accentColor: Colors.deepOrange,
              fontFamily: 'Lato',
              pageTransitionsTheme: PageTransitionsTheme(builders: {
                TargetPlatform.android: CustomPageTransitionBuilder(),
                TargetPlatform.iOS: CustomPageTransitionBuilder(),
              }),
            ),
            home: auth.isAuth
                ? HomeScreen()
                : FutureBuilder(
                    future: auth.tryAutoLogin(),
                    builder: (context, snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? SplashScreen()
                            : AuthScreen(),
                  ),

            routes: {
              AuthScreen.routeName: (context) => AuthScreen(),
              ArticulosScreen.routeName: (context) => ArticulosScreen(),
            }, 
          );
        },
      ),
    );
  }
}
