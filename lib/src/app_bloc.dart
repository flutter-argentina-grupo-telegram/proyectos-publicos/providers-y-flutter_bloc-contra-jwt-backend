import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/auth/bloc.dart';
import 'repositories/usuario_repository.dart';
import 'screens/auth/auth_screen.dart';
import 'screens/auth/auth_screen_bloc.dart';
import 'screens/home_screen.dart';
import 'utilities/custom_route.dart';

class MyAppBloc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Bstr Gestión',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Colors.deepOrange,
          fontFamily: 'Lato',
          pageTransitionsTheme: PageTransitionsTheme(builders: {
            TargetPlatform.android: CustomPageTransitionBuilder(),
            TargetPlatform.iOS: CustomPageTransitionBuilder(),
          }),
        ),
        home: BlocProvider(
          create: (_) =>  AuthBloc(
            usuarioRepository: UsuarioRepository(),
          ),
          child: BlocListener<AuthBloc, AuthState> (
            listener: (context, state) {
              if (state is ErrorBlocState) {
                _showError(context, state.message);
              }
              if (state is SignInOkBlocState) {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => HomeScreen())
                );
              }
              if (state is InitialAuthState) {
                _tryAutoLogin(context);
              }
            },
            child: BlocBuilder<AuthBloc, AuthState> (
              builder: (context, state) {
                return 
                  AuthScreenBloc();
              },
            ),
          ),
        ),
        routes: {
          AuthScreen.routeName: (context) => AuthScreen(),
        }, 
    );
  }

  
  void _tryAutoLogin(BuildContext context) {
    BlocProvider.of<AuthBloc>(context).add(
      DoTryAutoLoginEvent(),
    );
  }

  void _showError(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

}
