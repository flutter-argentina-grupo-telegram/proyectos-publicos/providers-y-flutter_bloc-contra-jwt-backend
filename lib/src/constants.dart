class Constants {

  static final String baseUrl = 'http://nico4kd-001-site3.etempurl.com';
  // static final String baseUrl = 'http://10.0.2.2:25835';
  static final String baseUrlApi = '$baseUrl/api';

  static final String userDataKey = 'userDataKey';


}